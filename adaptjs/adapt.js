/*
Adaptype.js
Adaptype.js 取自“adapt”+“type”，切图网（www.qietu.com）全网首发。
 *：aming <aming@qietu.com>
 * Licensed under the Apache License v2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 */

function adaptype() {
    $("body").width() > 1250 && $("body").width() < /*1440*/1921 ? ($("html").removeClass("desktop flatbed phone hd"), $("html").addClass("hd")) : $("body").width() > 980 && $("body").width() < 1300 ? ($("html").removeClass("desktop flatbed phone hd"), $("html").addClass("desktop")) : $("body").width() >= 768 && $("body").width() < 960 ? ($("html").removeClass("desktop flatbed phone hd"), $("html").addClass("flatbed")) : $("body").width() < 768 && ($("html").removeClass("desktop flatbed phone hd"), $("html").addClass("phone"))
}
$(function() {
    adaptype(),
    $(window).resize(function() {
        adaptype()
    }),
    $.browser.msie ? ($.browser.msie && $.browser.version == "6.0" && !$.support.style && $("html").addClass("ie6"), $.browser.msie && $.browser.version == "7.0" && !$.support.style && $("html").addClass("ie7"), $.browser.msie && $.browser.version == "8.0" && !$.support.style && $("html").addClass("ie8")) : $.browser.safari ?  $("html").addClass("safari") : $.browser.mozilla ? $("html").addClass("firefox") : $.browser.opera &&  $("html").addClass("opera")
})